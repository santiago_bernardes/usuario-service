package com.hello3.controllers;

import com.hello3.controllers.usuariomapper.usuariomapper.UsuarioMapper;
import com.hello3.controllers.usuariomodel.UsuarioRequest;
import com.hello3.controllers.usuariomodel.UsuarioResponse;
import com.hello3.exception.ErrorModel;
import com.hello3.model.Usuario;
import com.hello3.services.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping ("/usuarios")
@Api (value = "cadastro", description = "Possible operations related to usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioServiceImpl;

    @GetMapping(path = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UsuarioResponse> listarUsuarios(@RequestParam(value = "id", required = false) Long id,
                                                @RequestParam(value = "age", required = false) Integer idade) {
        List<Usuario> usuarios = usuarioServiceImpl.listarUsuarios(id, idade);
        return usuarios.stream().map(UsuarioMapper::convertToResponse).collect(Collectors.toList());
    }

    @PostMapping(path = "/inserir", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse adicionarUsuario(@RequestBody @Valid UsuarioRequest body){

        Usuario usuario = usuarioServiceImpl.adicionarUsuario(UsuarioMapper.convertToUsuario(body));
        return UsuarioMapper.convertToResponse(usuario);
    }


    @PutMapping(path = "/editar/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse atualizarUsuario(@PathVariable(value = "id") Long id,
                                            @RequestBody UsuarioRequest body) {
        Usuario usuario = usuarioServiceImpl.atualizarUsuario(id, UsuarioMapper.convertToUsuario(body));
        return UsuarioMapper.convertToResponse(usuario);
    }

    @DeleteMapping(path = "/deletar/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String deletarUsuario(@PathVariable(value = "id") Long id){
        return usuarioServiceImpl.deletarUsuario(id);
    }
}
