package com.hello3.controllers.usuariomodel;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by santiago on 21/09/17.
 */
@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class UsuarioResponse {

    @ApiModelProperty(notes = "The user's id number")
    private Long id;
    @ApiModelProperty (notes = "The user's name")
    private String name;
    @ApiModelProperty (notes = "The user's age")
    private Integer age;
    @ApiModelProperty (notes = "The user's genre")
    private String genre;
    @ApiModelProperty (notes = "The user's marital status")
    private String maritalStatus;
}
