package com.hello3.controllers.usuariomapper.usuariomapper;

import com.hello3.controllers.usuariomodel.UsuarioRequest;
import com.hello3.controllers.usuariomodel.UsuarioResponse;
import com.hello3.model.Usuario;

/**
 * Created by santiago on 21/09/17.
 */
public class UsuarioMapper {

    public static UsuarioResponse convertToResponse(Usuario usuario){
        if (usuario == null) return null;
        return UsuarioResponse.builder().id(usuario.getId()).name(usuario.getName()).age(usuario.getAge()).genre(usuario.getGenre()).
                maritalStatus(usuario.getMaritalStatus()).build();
    }

    public static Usuario convertToUsuario(UsuarioRequest usuarioRequest){
        if (usuarioRequest == null) return null;
        return Usuario.builder().name(usuarioRequest.getName()).age(usuarioRequest.getAge()).genre(usuarioRequest.getGenre())
                .maritalStatus(usuarioRequest.getMaritalStatus()).build();
    }
}
