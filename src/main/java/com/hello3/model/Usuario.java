package com.hello3.model;


import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data @NoArgsConstructor @AllArgsConstructor
@Builder
public class Usuario {

    @ApiModelProperty (notes = "The user's id number")
    private Long id;
    @ApiModelProperty (notes = "The user's name")
    private String name;
    @ApiModelProperty (notes = "The user's age")
    private Integer age;
    @ApiModelProperty (notes = "The user's genre")
    private String genre;
    @ApiModelProperty (notes = "The user's marital status")
    private String maritalStatus;

    public Usuario(String name, Integer age, String genre, String maritalStatus) {
        this.name = name;
        this.age = age;
        this.genre = genre;
        this.maritalStatus = maritalStatus;

    }
}

