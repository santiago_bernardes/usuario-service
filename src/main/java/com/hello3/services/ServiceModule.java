package com.hello3.services;

import com.hello3.data.UsuarioData;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceModule {

    @Bean
    public UsuarioService usuarioService(UsuarioData usuarioData){
        return new UsuarioService(usuarioData);
    }
}
