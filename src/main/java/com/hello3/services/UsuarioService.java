package com.hello3.services;

import com.hello3.exception.*;
import com.hello3.model.Usuario;
import com.hello3.data.UsuarioData;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UsuarioService {

    private UsuarioData usuarioData;

    public List<Usuario> listarUsuarios(Long id, Integer idade) {

        List<Usuario> usuarios = usuarioData.listarUsuarios();

        Predicate<Usuario> usuarioPredicate = usuario ->
                (id == null ? true : id.equals(usuario.getId())) && (idade == null ? true : idade.equals(usuario.getAge()));

        if(usuarios.stream().noneMatch(usuarioPredicate)) throw new UsuarioNotFoundException();
        return usuarios.stream().filter(usuarioPredicate).collect(Collectors.toList());
    }

    public Usuario adicionarUsuario(Usuario body) {

        Usuario novoUsuario = new Usuario(body.getName(), body.getAge(), body.getGenre(), body.getMaritalStatus());
        usuarioData.inserirNovoUsuario(novoUsuario);

        return novoUsuario;
    }

    public Usuario atualizarUsuario(Long id, Usuario body) {
        Usuario usuarioAtualizado = usuarioData.getUsuario(id);

        if (usuarioAtualizado == null) throw new UsuarioNotFoundException();

        Optional.ofNullable(body.getName()).ifPresent(usuarioAtualizado::setName);
        Optional.ofNullable(body.getAge()).ifPresent(usuarioAtualizado::setAge);
        Optional.ofNullable(body.getGenre()).ifPresent(usuarioAtualizado::setGenre);
        Optional.ofNullable(body.getMaritalStatus()).ifPresent(usuarioAtualizado::setMaritalStatus);
        usuarioAtualizado.setId(id);
        usuarioData.atualizarUsuario(usuarioAtualizado);

        return usuarioAtualizado;
    }

    public String deletarUsuario(Long id) {
        boolean delete = usuarioData.deletarUsuario(id);
        if (!delete) throw new UsuarioNotFoundException();
        else return "Usuário deletado!";
    }
}



