package com.hello3.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by santiago on 05/09/17.
 */
public class UsuarioNotFoundException extends ApiException {

    public String message;
    private String details;

    public UsuarioNotFoundException(String message, String details) {
        this.message = message;
        this.details = details;
    }

    public UsuarioNotFoundException() {
        message = "Usuário não encontrado";
        details = "Você pesquisou um usuário não existente";
    }

    public String getMessage() {
        return message;
    }

    public String getDetail() {
        return details;
    }

    @Override
    public HttpStatus httpStatus() {
        return HttpStatus.NOT_FOUND;
    }
}
