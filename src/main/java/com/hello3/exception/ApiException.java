package com.hello3.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by santiago on 08/09/17.
 */
public abstract class ApiException extends RuntimeException {

    public abstract String getMessage();
    public abstract String getDetail();
    public abstract HttpStatus httpStatus();

}
