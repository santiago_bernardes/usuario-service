package com.hello3.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by santiago on 06/09/17.
 */
@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(ApiException.class)
    @ResponseBody
    public ResponseEntity<ErrorModel> processUE(ApiException ue) {
        return ResponseEntity.status(ue.httpStatus())
                .body(new ErrorModel(ue.getMessage(), ue.getDetail()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ResponseEntity<List<ErrorModel>> processExc(MethodArgumentNotValidException exc) {
        List<FieldError> fieldErrors = exc.getBindingResult().getFieldErrors();
        return ResponseEntity.badRequest().body(fieldErrors.stream()
                .map(fieldError -> new ErrorModel(fieldError.getField(), fieldError.getDefaultMessage()))
                .collect(Collectors.toList()));
    }
}
