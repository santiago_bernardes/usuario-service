package com.hello3.exception;

/**
 * Created by santiago on 05/09/17.
 */
public class ErrorModel {

    private String message;
    private String details;

    public ErrorModel() {
    }

    public ErrorModel(String message, String details) {
        this.message = message;
        this.details = details;
    }

    public ErrorModel(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
