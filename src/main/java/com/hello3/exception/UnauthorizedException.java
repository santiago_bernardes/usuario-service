//package com.hello3.exception;
//
//import org.springframework.http.HttpStatus;
//
//public class UnauthorizedException extends ApiException {
//
//    private String message;
//    private String details;
//
//    public UnauthorizedException(String message, String details) {
//        this.message = message;
//        this.details = details;
//    }
//
//    public UnauthorizedException() {
//        message = "Não autorizado";
//        details = "Não foi possível realizar a operação pois as credenciais estão inválidas.";
//    }
//
//    @Override
//    public String getMessage() {
//        return message;
//    }
//
//    @Override
//    public String getDetail() {
//        return details;
//    }
//
//    @Override
//    public HttpStatus httpStatus() {
//        return HttpStatus.UNAUTHORIZED;
//    }
//}
