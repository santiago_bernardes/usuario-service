//package com.hello3.exception;
//
//import org.springframework.http.HttpStatus;
//
///**
// * Created by santiago on 06/09/17.
// */
//public class BadRequestException extends ApiException {
//
//    private String message;
//    private String details;
//
//    public BadRequestException(String message, String details) {
//        this.message = message;
//        this.details = details;
//    }
//
//    public BadRequestException() {
//        message = "Campo de autorização não pode ser nulo";
//        details = "Para adicionar um usuário deve ser inserido um campo de autorização no corpo da mensagem";
//    }
//
//    @Override
//    public String getMessage() {
//        return message;
//    }
//
//    public String getDetail() {
//        return details;
//    }
//
//    @Override
//    public HttpStatus httpStatus() {
//        return HttpStatus.BAD_REQUEST;
//    }
//}
