package com.hello3.data;

//import com.hello3.model.Autorizacao;
import com.hello3.model.Usuario;
import org.apache.ibatis.annotations.*;

import java.util.List;


public interface UsuarioData {

    @Select("select * from usuarios where id_usuario = #{id}")
    @Results(value = {
            @Result(column = "id_usuario", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(column = "age", property = "age"),
            @Result(column = "genre", property = "genre"),
            @Result(column = "marital_status", property = "maritalStatus")
    })
    public Usuario getUsuario(@Param("id") Long id);

    @Select("select exists (select * from usuarios where id_usuario = #{id})")
    public Boolean usuarioExiste(@Param("id") Long id);

    @Select("select * from usuarios")
    @Results(value = {
            @Result(column = "id_usuario", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(column = "age", property = "age"),
            @Result(column = "genre", property = "genre"),
            @Result(column = "marital_status", property = "maritalStatus")
    })
    public List<Usuario> listarUsuarios();

    @Insert("INSERT INTO usuarios " +
            "(name,age,genre,marital_status) " +
            "VALUES " +
            "(#{novoUsuario.name}, #{novoUsuario.age}, #{novoUsuario.genre}, #{novoUsuario.maritalStatus})")
    @Options(keyColumn = "id_usuario", useGeneratedKeys = true, keyProperty = "novoUsuario.id")
    public void inserirNovoUsuario(@Param("novoUsuario") Usuario novoUsuario);


    @Update("UPDATE usuarios SET name = #{usuarioAtualizado.name}, age = #{usuarioAtualizado.age}, " +
            "genre = #{usuarioAtualizado.genre}, marital_status = #{usuarioAtualizado.maritalStatus} " +
            "WHERE id_usuario = #{usuarioAtualizado.id}")
    public Boolean atualizarUsuario(@Param("usuarioAtualizado") Usuario usuarioAtualizado);


    @Update("DELETE from usuarios WHERE id_usuario = #{id} ")
    public Boolean deletarUsuario(@Param("id") Long id);
}
