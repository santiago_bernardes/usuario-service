//package hello3.controllers;
//
//import com.hello3.Application;
//import com.hello3.exception.BadRequestException;
//import com.hello3.exception.UnauthorizedException;
//import com.hello3.model.Usuario;
//import com.hello3.services.UsuarioServiceImpl;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.embedded.LocalServerPort;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.RequestEntity;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.annotation.DirtiesContext;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.net.URI;
//import java.util.Arrays;
//import java.util.List;
//
//import static org.hamcrest.Matchers.is;
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertThat;
//import static org.mockito.Mockito.when;
//
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
//public class UsuarioControllerTest {
//
//
//    @Autowired
//    private TestRestTemplate restTemplate;
//
//    @MockBean
//    private UsuarioServiceImpl usuarioService;
//
//    @LocalServerPort
//    private int port;
//    private String host;
//
//
//    @Before
//    public void setUp() throws Exception {
//        host = "http://localhost:" + port + "/usuarios";
//    }
//
//    @Test
//    public void listarUsuarios() throws Exception {
//        List<Usuario> lista = Arrays.asList(Usuario.builder().id(1L).nome("Joselito").idade(21).auth("123")
//                .estadoCivil("Solteiro").sexo("Masculino").build());
//        when(usuarioService.listarUsuarios(null, null)).thenReturn(lista);
//
//        ResponseEntity<Usuario[]> entity = restTemplate.getForEntity(host, Usuario[].class);
//
//        assertThat(entity.getStatusCodeValue(), is(200));
//        assertThat(lista.size(), is(1));
//    }
//
//    @Test
//    public void adicionarUsuario() throws Exception {
//        Usuario body = Usuario.builder().id(1l).nome("Jão").idade(22).sexo("M").estadoCivil("S").auth("123").build();
//        when(usuarioService.adicionarUsuario(body)).thenReturn(body);
//
//        ResponseEntity<Usuario> responseEntity = restTemplate.postForEntity(host, body, Usuario.class);
//
//        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
//        assertThat(body.getMaritalStatus(), is("S"));
//    }
//
//    @Test
//    public void adicionarUsuarioSemAuth() throws Exception {
//        Usuario body = Usuario.builder().id(1L).nome("Jão").idade(12).sexo("M").estadoCivil("S").build();
//        when(usuarioService.adicionarUsuario(body)).thenThrow(BadRequestException.class);
//
//        ResponseEntity<Usuario> responseEntity = restTemplate.postForEntity(host, body, Usuario.class);
//        assertEquals(responseEntity.getStatusCode(), HttpStatus.BAD_REQUEST);
//    }
//
//    @Test
//    @DirtiesContext
//    public void atualizarUsuario() throws Exception {
//        Usuario body = Usuario.builder().nome("Jão").build();
//        when(usuarioService.atualizarUsuario(1l, "123", body)).thenReturn(body);
//
//        RequestEntity<Usuario> requestEntity = RequestEntity.put(URI.create(host + "/1"))
//                .header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
//                .header("authorization", "123")
//                .body(body);
//
//        ResponseEntity<Usuario> responseEntity = restTemplate.exchange(requestEntity, Usuario.class);
//        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
//    }
//
//    @Test
//    public void deletarUsuario() throws Exception {
//        when(usuarioService.deletarUsuario(1L, "123")).thenReturn(null);
//
//        RequestEntity<Void> requestEntity = RequestEntity.delete(URI.create(host + "/1"))
//                .header("authorization", "123")
//                .header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
//                .build();
//
//        ResponseEntity<Usuario> responseEntity = restTemplate.exchange(requestEntity, Usuario.class);
//        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
//    }
//
//    @Test
//    public void deletarUsuarioComSenhaIncorreta() throws Exception {
//        when(usuarioService.deletarUsuario(1L, "123")).thenThrow(UnauthorizedException.class);
//
//        RequestEntity<Void> requestEntity = RequestEntity.delete(URI.create(host + "/1"))
//                .header("authorization", "123")
//                .header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
//                .build();
//
//        ResponseEntity<Void> responseEntity = restTemplate.exchange(requestEntity, Void.class);
//        assertEquals(responseEntity.getStatusCode(), HttpStatus.UNAUTHORIZED);
//    }
//}