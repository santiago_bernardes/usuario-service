//package hello3.controllers.services;
//
//import com.hello3.exception.BadRequestException;
//import com.hello3.exception.UnauthorizedException;
//import com.hello3.exception.UsuarioNotFoundException;
//import com.hello3.model.Usuario;
//import com.hello3.data.UsuarioData;
//import com.hello3.services.ServiceModule;
//import com.hello3.services.UsuarioServiceImpl;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Matchers;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.Arrays;
//import java.util.List;
//
//import static org.hamcrest.CoreMatchers.is;
//import static org.hamcrest.CoreMatchers.nullValue;
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertThat;
//import static org.mockito.Matchers.eq;
//import static org.mockito.Matchers.notNull;
//import static org.mockito.Mockito.doThrow;
//import static org.mockito.Mockito.when;
//
////@RunWith(MockitoJUnitRunner.class)
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = {ServiceModule.class})
//public class UsuarioServiceImplTest {
//
//    @Autowired
//    UsuarioServiceImpl usuarioService;
//
//    @MockBean
//    UsuarioData usuarioData;
//
//
//    /**
//     * JUnit
//     * Hamcrest
//     * Mockito
//     */
//
//    @Test
//    public void deveRetornarTodosUsuarios() {
//        when(usuarioData.listarUsuarios()).thenReturn(Arrays.asList(
//                Usuario.builder().id(1L).nome("Ze").estadoCivil("voando").idade(21).build(),
//                Usuario.builder().id(2L).nome("Fulano").sexo("Indefinido").idade(20).build(),
//                Usuario.builder().id(3L).nome("Jamil").estadoCivil("Casado").idade(23).build()
//        ));
//
//        List<Usuario> usuarios = usuarioService.listarUsuarios(null, null);
//        assertThat(usuarios.size(), is(3));
//    }
//
//    @Test
//    public void deveRetornarUmUsuario() throws Exception {
//        when(usuarioData.listarUsuarios()).thenReturn(Arrays.asList(
//                Usuario.builder().id(1l).nome("Zé").sexo("Masculino").idade(22).build(),
//                Usuario.builder().id(2L).nome("Jão").sexo("Masculino").idade(22).build(),
//                Usuario.builder().id(3l).nome("Jamil").sexo("Masculino").idade(21).build()
//        ));
//
//        List<Usuario> usuarios = usuarioService.listarUsuarios(null, 22);
//        assertThat(usuarios.size(), is(2));
//    }
//
//    @Test (expected = UsuarioNotFoundException.class)
//    public void deveRetornarNenhumUsuario() throws Exception {
//        when(usuarioData.listarUsuarios()).thenReturn(Arrays.asList(
//                Usuario.builder().id(1l).nome("Zé").sexo("Masculino").idade(22).build(),
//                Usuario.builder().id(2L).nome("Jão").sexo("Masculino").idade(22).build(),
//                Usuario.builder().id(3l).nome("Jamil").sexo("Masculino").idade(21).build()
//        ));
//
//        List<Usuario> usuarios = usuarioService.listarUsuarios(4l, null);
//    }
//
//    @Test
//    public void adicionarUsuarioComAuth() throws Exception {
//        Usuario novoUsuario = usuarioService.adicionarUsuario(
//                Usuario.builder().nome("Jão").idade(20).sexo("M").estadoCivil("S").auth("123").build());
//
//        assertThat(novoUsuario.getName(), is("Jão"));
//        assertThat(novoUsuario.getAge(), is(20));
//        assertThat(novoUsuario.getGenre(), is("M"));
//        assertThat(novoUsuario.getMaritalStatus(), is("S"));
//        assertThat(novoUsuario.getAuth(), is("123"));
//
//    }
//
//    @Test (expected = BadRequestException.class)
//    public void adicionarUsuarioSemAuth() throws Exception {
//        Usuario novoUsuario = usuarioService.adicionarUsuario(
//                Usuario.builder().nome("Jão").sexo("M").build());
//
//    }
//
//    @Test
//    public void atualizarUsuarioComSenha() throws Exception {
//        when(usuarioData.getUsuario(1L)).thenReturn(Usuario.builder().id(1L).nome("Jão").idade(20).sexo("M").estadoCivil("S").auth("123").build());
//        when(usuarioData.atualizarUsuarioComSenha(Usuario.builder().id(1L).nome("Jãoz").idade(20).sexo("M").estadoCivil("S").auth("123").build(),
//                "123")).thenReturn(true);
//
//        Usuario usuario = usuarioService.atualizarUsuario(1L, "123", Usuario.builder().nome("Jãoz").idade(20)
//                .build());
//
//        assertEquals(usuario.getGenre(), "M");
//        assertEquals(usuario.getName(), "Jãoz");
//        assertEquals(usuario.getMaritalStatus(), "S");
//        assertThat(usuario.getAge(), is(20));
//
//    }
//
//    @Test (expected = UnauthorizedException.class)
//    public void atualizarUsuarioComSenhaIncorreta() throws Exception {
//        when(usuarioData.getUsuario(1L)).thenReturn(Usuario.builder().id(1L).nome("Jão").idade(20).sexo("M").estadoCivil("S").auth("123").build());
//        when(usuarioData.atualizarUsuarioComSenha(Usuario.builder().id(1L).nome("Jão").idade(20).sexo("M").estadoCivil("S").auth("123").build(),
//                "123")).thenReturn(true);
//
//       usuarioService.atualizarUsuario(1L, "111", Usuario.builder().nome("Jão").idade(20)
//                .sexo("M").estadoCivil("S").auth("123").build());
//
//
//    }
//
//    @Test (expected = UsuarioNotFoundException.class)
//    public void atualizarUsuarioInexistente() throws Exception {
//        when(usuarioData.getUsuario(1L)).thenReturn(null);
//
//        usuarioService.atualizarUsuario(1L, "111", Usuario.builder().nome("Jão").idade(20)
//                .sexo("M").estadoCivil("S").auth("123").build());
//    }
//
//    @Test
//    public void naoAtualizarNull() throws Exception {
//        when(usuarioData.getUsuario(1L)).thenReturn(Usuario.builder().id(1L).nome("Jão").idade(20).sexo("M").estadoCivil("S").auth("123").build());
//        when(usuarioData.atualizarUsuarioComSenha(Matchers.any(), eq("123"))).thenReturn(true);
//
//        Usuario usuario = usuarioService.atualizarUsuario(1L, "123", Usuario.builder().nome("Josney").build());
//
//        assertThat(usuario.getAge(), is(20));
//        assertEquals(usuario.getName(), "Josney");
//        assertEquals(usuario.getMaritalStatus(), "S");
//    }
//
//    @Test (expected = UnauthorizedException.class)
//    public void deletarUsuarioComSenhaIncorreta() throws Exception {
//        Usuario usuario = Usuario.builder().id(1l).nome("jão").idade(20).auth("123").build();
//        when(usuarioData.getUsuario(1L)).thenReturn(usuario);
//        when(usuarioData.deletarUsuarioComSenha(1l, "123")).thenReturn(true);
//
//        usuarioService.deletarUsuario(1l, "111");
//    }
//
//    @Test
//    public void deletarUsuarioComSenha() throws Exception {
//        Usuario usuario = Usuario.builder().id(1l).nome("jão").idade(20).auth("123").build();
//        when(usuarioData.getUsuario(1L)).thenReturn(usuario);
//        when(usuarioData.deletarUsuarioComSenha(1l, "123")).thenReturn(true);
//
//        usuario = usuarioService.deletarUsuario(1l, "123");
//
//        assertEquals(usuario, null);
//    }
//
//    @Test (expected = UsuarioNotFoundException.class)
//    public void deletarUsuarioInexistente() throws Exception {
//        when(usuarioData.getUsuario(1L)).thenReturn(null); usuarioService.deletarUsuario(1l, "123");
//
//    }
//
//}